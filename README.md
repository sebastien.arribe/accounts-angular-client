# Accounts

Showcase project for managing user bank accounts and their transactions.

## Description
The project is an Angular client that calls the rest API defined [here](https://gitlab.com/sebastien.arribe/accounts-rest-api/).
This UI allows to list the users, their transasactions and to create new transactions.

## Installation
The project is Angular based; to run it: 

### Clone the Project
clone the repository: 
`git clone https://gitlab.com/sebastien.arribe/accounts-angular-client.git`

### Run the Project Locally 
1. go to the repository `accounts-angular-client`

2. run the angular client:
`ng serve`

3. access the client
Navigate to `http://localhost:4200/`

## Usage
TODO

## License
Can be used for educationnal purposes only.
May not be sold.
