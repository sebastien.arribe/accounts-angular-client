export class Transaction {

  constructor(
    private _id : number,
    private _emitter: number,
    private _recipient : number,
    private _amount : number
  ){}


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get emitter(): number {
    return this._emitter;
  }

  set emitter(value: number) {
    this._emitter = value;
  }

  get recipient(): number {
    return this._recipient;
  }

  set recipient(value: number) {
    this._recipient = value;
  }

  get amount(): number {
    return this._amount;
  }

  set amount(value: number) {
    this._amount = value;
  }
}
