import {Component, OnDestroy, OnInit} from '@angular/core';
import {AccountService} from "../services/account.service";
import {Account} from "../models/account";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-list-accounts',
  templateUrl: './list-accounts.component.html',
  styleUrls: ['./list-accounts.component.css']
})
export class ListAccountsComponent implements OnInit {

  accounts? : Account[];
  accountsSubscription? : Subscription;

  constructor(private service : AccountService) {

  }

  ngOnInit(): void {
    this.service.getAccounts().subscribe({
      next : accounts => this.accounts = accounts,
      error: err => console.log(console.error(err)),
      complete : () => console.log('chargment complet')
    })
  }

}
