import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { ListAccountsComponent } from './list-accounts/list-accounts.component';
import { AddAccountComponent } from './add-account/add-account.component';
import {AccountService} from "./services/account.service";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    ListAccountsComponent,
    AddAccountComponent
  ],
  providers:
    [AccountService],
  exports: [
    ListAccountsComponent,
    AddAccountComponent
  ],
  imports: [
    CommonModule,
    AccountRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class AccountModule { }
