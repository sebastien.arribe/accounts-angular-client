import {Injectable} from '@angular/core';
import {Account} from "../models/account";
import {Observable, Subject} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class AccountService {

  private _accounts: Account [] = [];

  accountUpdatedEvent = new Subject<Account[]>();
  private basicUrl: string = "http://localhost:8080";

  constructor(private http: HttpClient) {

  }

  getAccounts(): Observable<Account[]> {
    return this.http.get<Account[]>(`${this.basicUrl}/accounts`);
  }

  set accounts(value: Account[]) {
    this.accounts = value;
  }

  getAccountById(id: number): Account | undefined {
    return this.accounts.find(b => b.id === id)
  }

  addAccount(account: { balance: number; surname: string; name: string; id: number }): Observable<Account> {
    console.log('addAccount Method')
    const options = {
        headers: new HttpHeaders({'content-type': 'application/json'})
      };
    return this.http.post<Account>(`${this.basicUrl}/accounts/add`, Account, options);
  }

}
