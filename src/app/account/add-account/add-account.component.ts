import {Component} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AccountService} from '../services/account.service';
import {Router} from '@angular/router';
import {Transaction} from "../models/transaction";

@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.css']
})
export class AddAccountComponent {

  addAccountForm = this.fb.group(
    {
      id:[0],
      name: ['', Validators.required],
      surname: ['', Validators.required],
      balance: ['']
    }
  )

  constructor(
    private service: AccountService,
    private fb: FormBuilder,
    private router: Router
  ) {
  }

  addAccount = () => {

    this.service.addAccount({
      id : +this.addAccountForm.value.id!,
      name : this.addAccountForm.value.name!,
      surname : this.addAccountForm.value.surname!,
      balance : +this.addAccountForm.value.balance!
      // transactions : [new Transaction()]
    }
    ).subscribe(() =>
        this.router.navigate(['/accounts']));

  }
}
