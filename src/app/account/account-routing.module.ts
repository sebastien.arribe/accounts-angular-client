import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListAccountsComponent} from "./list-accounts/list-accounts.component";
import {AddAccountComponent} from "./add-account/add-account.component";

const routes: Routes = [
  {path:'', component: ListAccountsComponent},
  {path:'add', component: AddAccountComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
