import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AddAccountComponent} from "./account/add-account/add-account.component";

const routes: Routes = [{ path: 'account/add', component: AddAccountComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
